import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { Actor } from '@entities/actor.entity';

import { ActorService } from './actor.service';
import { CreateActorArgs } from './dto/create-actor.args';
import { UpdateActorArgs } from './dto/update-actor.args';

@Resolver()
export class ActorResolver {
	constructor(private readonly actorService: ActorService) {}
	@Query(() => [Actor])
	actors() {
		return this.actorService.find();
	}
	@Query(() => Actor, { nullable: true })
	actor(@Args('id') id: string) {
		return this.actorService.findOneById(id);
	}

	@Mutation(() => Actor)
	createActor(@Args() args: CreateActorArgs) {
		return this.actorService.create(args);
	}
	@Mutation(() => Actor)
	deleteActor(@Args('id') id: string) {
		return this.actorService.delete(id);
	}
	@Mutation(() => Actor)
	updateActor(@Args() data: UpdateActorArgs) {
		return this.actorService.update(data);
	}
}
