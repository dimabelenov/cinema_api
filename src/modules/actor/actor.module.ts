import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Actor } from '@entities/actor.entity';

import { AwsModule } from '@modules/aws/aws.module';

import { ActorResolver } from './actor.resolver';
import { ActorService } from './actor.service';

@Module({
	imports: [TypeOrmModule.forFeature([Actor]), AwsModule],
	providers: [ActorResolver, ActorService],
})
export class ActorModule {}
