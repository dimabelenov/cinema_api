import { ArgsType, Field } from '@nestjs/graphql';

import { IFileUpload } from '@interfaces/file-upload.interface';
import { GraphQLUpload } from 'graphql-upload-minimal';

@ArgsType()
export class CreateActorArgs {
	@Field()
	first_name: string;
	@Field()
	last_name: string;
	@Field({ nullable: true })
	birth_date?: string;
	@Field(() => GraphQLUpload, { nullable: true })
	img?: Promise<IFileUpload>;
}
