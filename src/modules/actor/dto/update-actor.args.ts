import { ArgsType, Field } from '@nestjs/graphql';

import { IFileUpload } from '@interfaces/file-upload.interface';
import { GraphQLUpload } from 'graphql-upload-minimal';

@ArgsType()
export class UpdateActorArgs {
	@Field()
	id: string;
	@Field({ nullable: true })
	first_name?: string;
	@Field({ nullable: true })
	last_name?: string;
	@Field({ nullable: true })
	birth_date?: string;
	@Field(() => GraphQLUpload, { nullable: true })
	img?: Promise<IFileUpload>;
}
