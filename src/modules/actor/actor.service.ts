import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { Actor } from '@entities/actor.entity';

import { AwsS3Service } from '@modules/aws/aws-s3.service';

import { CreateActorArgs } from './dto/create-actor.args';
import { UpdateActorArgs } from './dto/update-actor.args';

@Injectable()
export class ActorService {
	constructor(
		@InjectRepository(Actor)
		private actorRepository: Repository<Actor>,
		private awsS3Service: AwsS3Service,
	) {}
	find() {
		return this.actorRepository.find();
	}
	findOneById(id: string) {
		return this.actorRepository.findOne({
			where: {
				id,
			},
		});
	}
	async create({ img, ...rest }: CreateActorArgs) {
		const response = await this.awsS3Service.uploadImage(img);
		if (response instanceof BadRequestException) {
			throw response;
		}

		const actor = this.actorRepository.create({
			...rest,
			img: response,
		});

		try {
			const { id } = await this.actorRepository.save(actor);
			return await this.findOneById(id);
		} catch (error) {
			throw new BadRequestException('Invalid input data', error.message);
		}
	}
	async delete(id: string) {
		const actor = await this.actorRepository.findOneBy({ id });
		if (!actor) {
			throw new NotFoundException('Actor not found');
		}
		try {
			await this.awsS3Service.removeImage(actor.img);
			await this.actorRepository.delete({ id });
		} catch (error) {
			throw new BadRequestException(error.message);
		}
		return actor;
	}

	async update({ img, id, ...args }: UpdateActorArgs) {
		const actor = await this.actorRepository.findOneBy({ id });
		if (!actor) {
			throw new NotFoundException('Actor not found');
		}
		let imgPath = null;

		if (img) {
			await this.awsS3Service.removeImage(actor.img);
			const response = await this.awsS3Service.uploadImage(img);

			if (response instanceof BadRequestException) {
				throw response;
			}
			imgPath = response;

			return this.actorRepository.update({ id }, { ...args, img: imgPath });
		} else {
			console.log(123, args);

			return this.actorRepository.update({ id }, { ...actor, ...args });
		}
	}
}
