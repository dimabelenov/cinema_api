import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { Category } from '@entities/category.entity';

import { MovieService } from '@modules/movie/movie.service';

import { CreateCategoryArgs } from './dto/create-category.args';

@Injectable()
export class CategoryService {
	constructor(
		@InjectRepository(Category)
		private categoryRepository: Repository<Category>,
		private movieService: MovieService,
	) {}

	find() {
		return this.categoryRepository.find();
	}

	async create(data: CreateCategoryArgs) {
		try {
			const category = this.categoryRepository.create({
				...data,
			});
			return await this.categoryRepository.save(category);
		} catch (error) {
			throw new BadRequestException('Invalid input data', error.message);
		}
	}
	findMoviesByCategory(categoryId: string) {
		return this.movieService.findMoviesByCategory(categoryId);
	}

	async delete(id: string) {
		const category = await this.categoryRepository.findOneBy({ id });
		if (!category) {
			throw new NotFoundException('Category not found');
		}
		try {
			await this.categoryRepository.delete(id);
		} catch (error) {
			throw new BadRequestException(error.message);
		}
		return category;
	}
}
