import { Args, Mutation, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';

import { Category } from '@entities/category.entity';
import { Movie } from '@entities/movie.entity';

import { CategoryService } from './category.service';
import { CreateCategoryArgs } from './dto/create-category.args';

@Resolver(() => Category)
export class CategoryResolver {
	constructor(private categoryService: CategoryService) {}

	@Query(() => [Category])
	categories() {
		return this.categoryService.find();
	}
	@ResolveField(() => [Movie], { nullable: true })
	movie(@Parent() category: Category) {
		return this.categoryService.findMoviesByCategory(category.id);
	}

	@Mutation(() => Category)
	createCategory(@Args() args: CreateCategoryArgs) {
		return this.categoryService.create(args);
	}

	@Mutation(() => Category)
	deleteCategory(@Args('id') id: string) {
		return this.categoryService.delete(id);
	}
}
