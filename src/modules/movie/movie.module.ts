import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Movie } from '@entities/movie.entity';

import { AwsModule } from '@modules/aws/aws.module';

import { MovieResolver } from './movie.resolver';
import { MovieService } from './movie.service';

@Module({
	imports: [TypeOrmModule.forFeature([Movie]), AwsModule],
	providers: [MovieResolver, MovieService],
	exports: [MovieService],
})
export class MovieModule {}
