import { ArgsType, Field, ID, InputType, Int } from '@nestjs/graphql';

import { IFileUpload } from '@interfaces/file-upload.interface';
import { GraphQLUpload } from 'graphql-upload-minimal';

@InputType()
class RelationInput {
	@Field(() => ID)
	id: string;
}

@ArgsType()
export class CreateMovieArgs {
	@Field()
	title: string;
	@Field(() => Int)
	year: number;
	@Field({ nullable: true })
	imdb?: number;
	@Field({ nullable: true })
	description?: string;
	@Field(() => GraphQLUpload, { nullable: true })
	img?: Promise<IFileUpload>;

	@Field(() => [RelationInput])
	categories: RelationInput[];

	@Field(() => [RelationInput], { nullable: true })
	actors?: RelationInput[];
}
