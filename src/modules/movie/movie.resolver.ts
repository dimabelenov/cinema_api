import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { Movie } from '@entities/movie.entity';

import { CreateMovieArgs } from './dto/create-movie.args';
import { MovieService } from './movie.service';

@Resolver()
export class MovieResolver {
	constructor(private readonly movieService: MovieService) {}
	@Query(() => [Movie])
	movies() {
		return this.movieService.find();
	}
	@Query(() => Movie)
	movie(@Args('id') id: string) {
		return this.movieService.findOneById(id);
	}
	@Mutation(() => Movie)
	createMovie(@Args() args: CreateMovieArgs) {
		return this.movieService.create(args);
	}
	@Mutation(() => Movie)
	deleteMovie(@Args('id') id: string) {
		return this.movieService.delete(id);
	}
}
