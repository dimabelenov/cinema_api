import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { Movie } from '@entities/movie.entity';

import { AwsS3Service } from '@modules/aws/aws-s3.service';

import { CreateMovieArgs } from './dto/create-movie.args';

@Injectable()
export class MovieService {
	constructor(
		@InjectRepository(Movie)
		private movieRepository: Repository<Movie>,
		private awsS3Service: AwsS3Service,
	) {}

	find() {
		return this.movieRepository.find({
			relations: {
				categories: true,
				actors: true,
			},
		});
	}
	findOneById(id: string) {
		return this.movieRepository.findOne({
			where: {
				id,
			},
			relations: {
				categories: true,
				actors: true,
			},
		});
	}
	async create({ img, ...rest }: CreateMovieArgs): Promise<Movie> {
		const response = await this.awsS3Service.uploadImage(img);
		if (response instanceof BadRequestException) {
			throw response;
		}

		const movie = this.movieRepository.create({
			...rest,
			img: response,
		});

		try {
			const { id } = await this.movieRepository.save(movie);
			return await this.findOneById(id);
		} catch (error) {
			throw new BadRequestException('Invalid input data', error.message);
		}
	}
	findMoviesByCategory(categoryId: string) {
		return this.movieRepository.find({
			where: {
				categories: {
					id: categoryId,
				},
			},
			relations: {
				categories: true,
			},
		});
	}

	findMovieById(id: string) {
		return this.movieRepository.findOne({
			where: {
				id,
			},
			relations: {
				categories: true,
				actors: true,
			},
		});
	}

	async delete(id: string) {
		const movie = await this.movieRepository.findOneBy({ id });
		if (!movie) {
			throw new NotFoundException('Movie not found');
		}
		try {
			await this.movieRepository.delete(id);
		} catch (error) {
			throw new BadRequestException(error.message);
		}
		return movie;
	}
}
