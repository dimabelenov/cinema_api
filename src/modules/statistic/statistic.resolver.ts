import { Query, Resolver } from '@nestjs/graphql';

import { Statistic } from '@entities/statistic.entity';

import { StatisticService } from './statistic.service';

@Resolver()
export class StatisticResolver {
	constructor(private readonly statisticService: StatisticService) {}
	@Query(() => Statistic)
	statistic() {
		return this.statisticService.getStatistic();
	}
}
