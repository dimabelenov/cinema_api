import { Test, TestingModule } from '@nestjs/testing';

import { StatisticResolver } from './statistic.resolver';

describe('StatisticResolver', () => {
	let resolver: StatisticResolver;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [StatisticResolver],
		}).compile();

		resolver = module.get<StatisticResolver>(StatisticResolver);
	});

	it('should be defined', () => {
		expect(resolver).toBeDefined();
	});
});
