import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Actor } from '@entities/actor.entity';
import { Category } from '@entities/category.entity';
import { Movie } from '@entities/movie.entity';

import { StatisticResolver } from './statistic.resolver';
import { StatisticService } from './statistic.service';

@Module({
	imports: [TypeOrmModule.forFeature([Actor, Movie, Category])],
	providers: [StatisticResolver, StatisticService],
})
export class StatisticModule {}
