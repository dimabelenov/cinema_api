import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { Actor } from '@entities/actor.entity';
import { Category } from '@entities/category.entity';
import { Movie } from '@entities/movie.entity';

@Injectable()
export class StatisticService {
	constructor(
		@InjectRepository(Actor)
		private actorRepository: Repository<Actor>,
		@InjectRepository(Movie)
		private movieRepository: Repository<Movie>,
		@InjectRepository(Category)
		private categoryRepository: Repository<Category>,
	) {}

	private getCountOfActors() {
		return this.actorRepository.count();
	}
	private getCountOfMovies() {
		return this.movieRepository.count();
	}
	private getCountOfCategories() {
		return this.categoryRepository.count();
	}
	getStatistic() {
		return {
			actors_count: this.getCountOfActors(),
			movies_count: this.getCountOfMovies(),
			categories_count: this.getCountOfCategories(),
		};
	}
}
