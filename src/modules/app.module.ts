import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';

import { join } from 'path';

import { ActorModule } from './actor/actor.module';
import { AwsModule } from './aws/aws.module';
import { CategoryModule } from './category/category.module';
import { ImageConverterModule } from './image-converter/image-converter.module';
import { MovieModule } from './movie/movie.module';
import { StatisticModule } from './statistic/statistic.module';

import configuration from '@config/configuration';

const { POSTGRES_HOST, POSTGRES_PORT, POSTGRES_DB, POSTGRES_PASSWORD, POSTGRES_USER, NODE_ENV } =
	configuration();

@Module({
	imports: [
		TypeOrmModule.forRoot({
			type: 'postgres',
			host: POSTGRES_HOST,
			port: POSTGRES_PORT,
			username: POSTGRES_USER,
			password: POSTGRES_PASSWORD,
			database: POSTGRES_DB,
			entities: [__dirname + '/../**/*.entity{.ts,.js}'],
			synchronize: NODE_ENV === 'develop' ? true : false,
		}),
		GraphQLModule.forRoot<ApolloDriverConfig>({
			driver: ApolloDriver,
			autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
			playground: false,
		}),
		ActorModule,
		CategoryModule,
		MovieModule,
		AwsModule,
		StatisticModule,
		ImageConverterModule,
	],
})
export class AppModule {}
