import { Module } from '@nestjs/common';

import { S3Client } from '@aws-sdk/client-s3';
import { ImageConverterModule } from '@modules/image-converter/image-converter.module';
import { AwsSdkModule } from 'aws-sdk-v3-nest';

import { AwsS3Service } from './aws-s3.service';

import configuration from '@config/configuration';

const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION } = configuration();
@Module({
	imports: [
		AwsSdkModule.register({
			client: new S3Client({
				region: AWS_REGION,
				credentials: {
					accessKeyId: AWS_ACCESS_KEY_ID,
					secretAccessKey: AWS_SECRET_ACCESS_KEY,
				},
			}),
		}),
		ImageConverterModule,
	],
	providers: [AwsS3Service],
	exports: [AwsS3Service],
})
export class AwsModule {}
