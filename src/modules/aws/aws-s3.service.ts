import { BadRequestException, Injectable } from '@nestjs/common';

import { DeleteObjectCommand, PutObjectCommand, S3Client } from '@aws-sdk/client-s3';
import { IFileUpload } from '@interfaces/file-upload.interface';
import { ImageConverterService } from '@modules/image-converter/image-converter.service';
import { InjectAws } from 'aws-sdk-v3-nest';
import { Stream } from 'node:stream';
import { parse } from 'path';

import configuration from '@config/configuration';

const { AWS_BUCKET_NAME } = configuration();
@Injectable()
export class AwsS3Service {
	constructor(
		@InjectAws(S3Client)
		private readonly s3: S3Client,
		private readonly imageConverterService: ImageConverterService,
	) {}
	private streamToBuffer(stream: Stream): Promise<Buffer> {
		return new Promise((resolve, reject) => {
			const chunks: Uint8Array[] = [];
			stream.on('data', (chunk) => chunks.push(chunk));
			stream.on('end', () => resolve(Buffer.concat(chunks)));
			stream.on('error', (error) => reject(error));
		});
	}

	private getUniqueFilename(filename: string) {
		return `${Date.now()}-${Math.random().toString(36).substring(2, 15)}-${filename}`;
	}

	isImage(mimetype: string) {
		return mimetype.startsWith('image');
	}

	async uploadImage(img: Promise<IFileUpload> | null) {
		if (!img) {
			return null;
		}

		const { filename, mimetype, createReadStream } = await img;
		if (!this.isImage(mimetype)) {
			return new BadRequestException('Invalid file type');
		}
		const fileContentBuffer = await this.streamToBuffer(createReadStream());
		const thumb = await this.imageConverterService.resizeImage(fileContentBuffer);
		const key = this.getUniqueFilename(filename);
		try {
			await this.s3.send(
				new PutObjectCommand({
					Bucket: AWS_BUCKET_NAME,
					Key: key,
					Body: thumb,
					ContentType: mimetype,
				}),
			);
			return `https://${AWS_BUCKET_NAME}.s3.amazonaws.com/${key}`;
		} catch (error) {
			return new BadRequestException(error.message);
		}
	}
	async removeImage(key: string) {
		if (!key) {
			return;
		}
		const fileName = parse(key).base;
		try {
			await this.s3.send(
				new DeleteObjectCommand({
					Bucket: AWS_BUCKET_NAME,
					Key: fileName,
				}),
			);
		} catch (error) {
			throw new BadRequestException(error.message);
		}
	}
}
