import { Field, ObjectType } from '@nestjs/graphql';

import {
	Column,
	CreateDateColumn,
	Entity,
	ManyToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

import { Movie } from './movie.entity';

@Entity()
@ObjectType()
export class Actor {
	@PrimaryGeneratedColumn('uuid')
	@Field()
	id: string;
	@Column()
	@Field()
	first_name: string;
	@Column()
	@Field()
	last_name: string;
	@Column({ nullable: true })
	@Field({ nullable: true })
	birth_date?: string;
	@Column({ nullable: true })
	@Field({ nullable: true })
	img?: string;
	@CreateDateColumn()
	@Field()
	created_at: Date;
	@UpdateDateColumn()
	@Field()
	updated_at: Date;

	@ManyToMany(() => Movie, (movie) => movie.actors, { onDelete: 'CASCADE' })
	movies: Movie[];
}
