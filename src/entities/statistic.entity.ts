import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Statistic {
	@Field()
	actors_count: number;
	@Field()
	movies_count: number;
	@Field()
	categories_count: number;
}
