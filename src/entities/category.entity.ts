import { Field, ObjectType } from '@nestjs/graphql';

import {
	Column,
	CreateDateColumn,
	Entity,
	ManyToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

import { Movie } from './movie.entity';

@Entity()
@ObjectType()
export class Category {
	@PrimaryGeneratedColumn('uuid')
	@Field()
	id: string;
	@Column({ unique: true })
	@Field()
	title: string;
	@CreateDateColumn()
	@Field()
	created_at: Date;
	@UpdateDateColumn()
	@Field()
	updated_at: Date;

	@ManyToMany(() => Movie, (movie) => movie.categories)
	movies: Movie[];
}
