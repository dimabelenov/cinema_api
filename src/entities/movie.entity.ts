import { Field, ObjectType } from '@nestjs/graphql';

import {
	Column,
	CreateDateColumn,
	Entity,
	JoinTable,
	ManyToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

import { Actor } from './actor.entity';
import { Category } from './category.entity';

@Entity()
@ObjectType()
export class Movie {
	@PrimaryGeneratedColumn('uuid')
	@Field()
	id: string;
	@Column()
	@Field()
	title: string;
	@Column()
	@Field()
	year: number;
	@Column({ type: 'float', nullable: true })
	@Field({ nullable: true })
	imdb?: number;
	@Column({ nullable: true })
	@Field({ nullable: true })
	description?: string;
	@Column({ nullable: true })
	@Field({ nullable: true })
	img?: string;
	@CreateDateColumn()
	@Field()
	created_at: Date;
	@UpdateDateColumn()
	@Field()
	updated_at: Date;

	@ManyToMany(() => Category, (category) => category.movies)
	@JoinTable({ name: 'movies_categories' })
	@Field(() => [Category])
	categories: Category[];

	@ManyToMany(() => Actor, (actor) => actor.movies)
	@JoinTable({ name: 'movies_actors' })
	@Field(() => [Actor])
	actors: Actor[];
}
