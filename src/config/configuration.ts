import { config } from 'dotenv';

config({ path: `.env.${process.env.NODE_ENV}` });

export default () => ({
	PORT: parseInt(process.env.PORT, 10),
	NODE_ENV: process.env.NODE_ENV,
	// DB
	POSTGRES_USER: process.env.POSTGRES_USER,
	POSTGRES_HOST: process.env.POSTGRES_HOST,
	POSTGRES_DB: process.env.POSTGRES_DB,
	POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD,
	POSTGRES_PORT: parseInt(process.env.POSTGRES_PORT, 10),
	// REDIS
	REDIS_PORT: parseInt(process.env.REDIS_PORT, 10),
	REDIS_HOST: process.env.REDIS_HOST,
	REDIS_TTL: parseInt(process.env.REDIS_TTL, 10) || 300,
	// AWS
	AWS_REGION: process.env.AWS_REGION,
	AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
	AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
	AWS_BUCKET_NAME: process.env.AWS_BUCKET_NAME,
});
