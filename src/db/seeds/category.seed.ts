import { DataSource } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';

import { Category } from '../../entities/category.entity';

const MOVIE_CATEGORIES = [
	'Action',
	'Comedy',
	'Drama',
	'Science Fiction',
	'Fantasy',
	'Horror',
	'Romance',
	'Thriller',
	'Documentary',
	'Adventure',
];

export default class CreateCategory implements Seeder {
	private generateCategories() {
		return MOVIE_CATEGORIES.map((c) => {
			const category: Partial<Category> = {
				title: c,
			};
			return category;
		});
	}
	public async run(factory: Factory, connection: DataSource) {
		await connection.createQueryBuilder().delete().from(Category).execute();

		await connection
			.createQueryBuilder()
			.insert()
			.into(Category)
			.values(this.generateCategories())
			.execute();
	}
}
