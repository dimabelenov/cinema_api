import { DataSource } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';

import { faker } from '@faker-js/faker';

import { Actor } from '../../entities/actor.entity';

export default class CreateActor implements Seeder {
	private generateActors(count: number) {
		return Array.from(Array(count).keys()).map(() => {
			const actor: Partial<Actor> = {
				first_name: faker.person.firstName(),
				last_name: faker.person.lastName(),
				img: faker.image.avatar(),
				birth_date: faker.date.birthdate().toString(),
			};
			return actor;
		});
	}
	public async run(factory: Factory, connection: DataSource) {
		await connection.createQueryBuilder().delete().from(Actor).execute();

		await connection
			.createQueryBuilder()
			.insert()
			.into(Actor)
			.values(this.generateActors(100))
			.execute();
	}
}
